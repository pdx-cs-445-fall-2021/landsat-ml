import tensorflow as tf
import numpy as np
from tensorflow.keras import layers, models
import matplotlib.pyplot as plt
import tensorflow.keras.optimizers as opt

root_path = "/Users/nick/dev/ml-2021/38-cloud-project/"

resolution = "48x48"
learning_rate = 0.001
batch_size = 20

training_images = np.load(root_path + resolution + "_training-images.npy")
training_targets = np.load(root_path + resolution + "_training-targets.npy")

validation_images = np.load(root_path + resolution + "_validation-images.npy")
validation_targets = np.load(root_path + resolution + "_validation-targets.npy")

testing_images = np.load(root_path + resolution + "_testing-images.npy")
testing_targets = np.load(root_path + resolution + "_testing-targets.npy")

convolution_size = (3, 3)
pool_size = (2, 2)

model = models.Sequential()

model.add(layers.Conv2D(8, convolution_size, padding='same', activation='relu', input_shape=training_images[0].shape))

model.add(layers.Conv2D(8, convolution_size, padding='same', activation='relu'))

model.add(layers.Conv2D(16, convolution_size, padding='same', activation='relu'))

model.add(layers.Conv2D(16, convolution_size, padding='same', activation='relu'))

model.add(layers.Conv2D(1, (1, 1), activation='sigmoid'))

model.compile(
    optimizer=opt.Adam(learning_rate=learning_rate),
    loss=tf.keras.losses.BinaryCrossentropy(),
    metrics=['accuracy'])

test_outputs = model.predict(training_images)

history = model.fit(
    training_images,
    training_targets,
    validation_data=(validation_images, validation_targets),
    epochs=100,
    batch_size=batch_size
)

evaluation = model.evaluate(
    testing_images,
    testing_targets,
    batch_size=batch_size,
    return_dict=True
)

plt.plot(history.history['accuracy'], label='training set')
plt.plot(history.history['val_accuracy'], label='validation set')
plt.title(
    'Resolution: {}, Learning rate: {}, Batch size: {}\nTest accuracy: {:.4f}, Test loss: {:.4f}'
    .format(resolution, learning_rate, batch_size, evaluation['accuracy'], evaluation['loss'])
)
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.7, 1.0])
plt.legend(loc='lower right')
plt.show()
