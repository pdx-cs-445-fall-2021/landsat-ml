import os
import numpy as np
import pandas as pd
from PIL import Image

IMAGE_SIZE = (10, 10)

root_path = "C:/Users/cfaber/cs545/cloud_dataset/"
training_path = root_path + "38-Cloud_training/"
testing_path = root_path + "38-Cloud_test/"

training_patches = pd.read_csv(training_path + 'training_patches_38-Cloud.csv', header=0)

blue_training_path = training_path + "train_blue/blue_"
green_training_path = training_path + "train_green/green_"
nir_training_path = training_path + "train_nir/nir_"
red_training_path = training_path + "train_red/red_"
gt_training_path = training_path + "train_gt/gt_"

blue_training_images = np.array(
    [np.array(Image.open(blue_training_path + path + ".TIF").resize(IMAGE_SIZE)) for path in training_patches.values.flatten()]
) / 65535

def loadImages(path, size, ext='.TIF'):
    patches_list = training_patches.values.flatten()
    loadedImages = []
    for p in patches_list:
        with open(path+p+ext, 'rb') as f:
            img = np.array(Image.open(f).resize(size))
            loadedImages.append(img)
    return np.array(loadedImages)

blue_training_images = loadImages(blue_training_path, IMAGE_SIZE) / 65535
green_training_images = loadImages(green_training_path, IMAGE_SIZE) / 65535
nir_training_images = loadImages(nir_training_path, IMAGE_SIZE) / 65535
red_training_images = loadImages(red_training_path, IMAGE_SIZE) / 65535
training_targets = loadImages(gt_training_path, IMAGE_SIZE) / 255

# blue_training_images = np.array(
#     [np.array(Image.open(blue_training_path + path + ".TIF").resize(IMAGE_SIZE)) for path in training_patches.values.flatten()]
# ) / 65535

training_images = np.moveaxis(np.array(
    (blue_training_images, green_training_images, nir_training_images, red_training_images)
), 0, 3)
training_targets = np.array(
    [np.array(Image.open(gt_training_path + path + ".TIF").resize(IMAGE_SIZE)) for path in training_patches.values.flatten()]
) / 255

# https://stackoverflow.com/a/4602224
def unison_shuffled_copies(a, b):
    assert len(a) == len(b)
    p = np.random.permutation(len(a))
    return a[p], b[p]

# shuffle the images and targets together
training_images, training_targets = unison_shuffled_copies(training_images, training_targets)

testing_images = training_images[0:1680]
validation_images = training_images[1680:2520]
training_images = training_images[2520:8400]

size_label = str(IMAGE_SIZE[0]) + 'x' + str(IMAGE_SIZE[0]) + '_'

np.save(root_path + size_label + "training-images.npy", training_images)
np.save(root_path + size_label + "validation-images.npy", validation_images)
np.save(root_path + size_label + "testing-images.npy", testing_images)

# add a dummy channel to the training targets
training_targets = training_targets[..., None]

# threshold the distortions from scaling
training_targets[training_targets >= 0.5] = 1
training_targets[training_targets < 0.5] = 0

testing_targets = training_targets[0:1680]
validation_targets = training_targets[1680:2520]
training_targets = training_targets[2520:8400]

np.save(root_path + size_label + "training-targets.npy", training_targets)
np.save(root_path + size_label + "validation-targets.npy", validation_targets)
np.save(root_path + size_label + "testing-targets.npy", testing_targets)

print("Done!")