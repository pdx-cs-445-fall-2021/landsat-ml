import numpy as np
import mlp
import idx2numpy as i2p
import matplotlib.pyplot as plt
import seaborn as sn
import pandas as pd
import os
from tqdm import tqdm
from skimage.io import imread
from skimage.transform import resize

if __name__ == '__main__':
    GLOBAL_PATH = '.'
    TRAIN_FOLDER = os.path.join(GLOBAL_PATH, './38-Cloud_training')
    train_patches_csv_name = 'training_patches_38-cloud.csv'
    df_train_img = pd.read_csv(os.path.join(TRAIN_FOLDER, train_patches_csv_name))
    imgs = np.zeros((8400,9216))
    msks = np.zeros((8400,2304))
    i = 0

    for filenames in tqdm(df_train_img['name'], miniters=1000):
        nred = 'red_' + filenames
        nblue = 'blue_' + filenames
        ngreen = 'green_' + filenames
        nnir = 'nir_' + filenames

        nmask = 'gt_' + filenames
        fl_msk = resize(imread(TRAIN_FOLDER + '/train_gt/' + '{}.TIF'.format(nmask)),(48,48), preserve_range=True, mode='symmetric')
        msks[i] = np.where((np.resize(np.array(fl_msk),(1,2304)) / 255) > 0, 0.9, 0.1)

        im_red = resize(imread(TRAIN_FOLDER + '/train_red/' + '{}.TIF'.format(nred)), (48,48), preserve_range=True, mode='symmetric')
        im_green = resize(imread(TRAIN_FOLDER + '/train_green/' + '{}.TIF'.format(ngreen)), (48,48), preserve_range=True, mode='symmetric')
        im_blue = resize(imread(TRAIN_FOLDER + '/train_blue/' + '{}.TIF'.format(nblue)), (48,48), preserve_range=True, mode='symmetric')
        im_nir = resize(imread(TRAIN_FOLDER + '/train_nir/' + '{}.TIF'.format(nnir)), (48,48), preserve_range=True, mode='symmetric')

        imgs[i,:2304] = np.resize(np.array(im_red),(1,2304))
        imgs[i,2304:4608] = np.resize(np.array(im_green),(1,2304))
        imgs[i,4608:6912] = np.resize(np.array(im_blue),(1,2304))
        imgs[i,6912:9216] = np.resize(np.array(im_nir),(1,2304))
        imgs[i] /= 255
        i += 1

    epochs = 50
    mlp = mlp.MLP(imgs, msks, 50, 0.01, 0.9)
    mlp.train(imgs, msks, epochs)

    print("ACCURACY: ", mlp.trainAccuracy)
    plt.plot(range(epochs+1), mlp.trainAccuracy)
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.show()







