import numpy as np


class MLP:
    def __init__(self, inputs, targets, hidden, eta, momentum):

        self.numInputs = np.shape(inputs)[1]
        self.numOutputs = np.shape(targets)[1]

        self.eta = eta
        self.momentum = momentum
        self.trainAccuracy = [0]
        self.testAccuracy = [0]
        self.numHidden = hidden
        self.numData = np.shape(inputs)[0]

        self.trainAccuracy = []
        self.testAccuracy = []
        self.hiddenWeights = (np.random.rand(self.numInputs + 1, self.numHidden) * 0.1) - 0.05
        self.outputWeights = (np.random.rand(self.numHidden + 1, self.numOutputs) * 0.1) - 0.05

    def train(self, inputs, targets, epochs):
        inputs = np.concatenate((inputs, np.ones((self.numData, 1))), axis=1)

        # These matrices hold the last iteration's weight updates
        deltaHidden = np.zeros((np.shape(self.hiddenWeights)))
        deltaOutput = np.zeros((np.shape(self.outputWeights)))

        self.trainAccuracy.append(self.get_accuracy(inputs, targets))

        for i in range(epochs):
            # Update weights with every data point
            for j in range(self.numData):
                self.outputs = self.forward([inputs[j]])

                # Error values for each hidden and output node
                output_error = self.outputs * (1 - self.outputs) * (self.outputs - [targets[j]])
                hidden_error = self.hidden * (1.0 - self.hidden) * (
                    np.dot(output_error, np.transpose(self.outputWeights)))

                # Adjust Weights and store value
                deltaHidden = self.eta * (
                    np.dot(np.transpose([inputs[j]]), hidden_error[:, :-1])) + self.momentum * deltaHidden
                deltaOutput = self.eta * (np.dot(np.transpose(self.hidden), output_error)) + self.momentum * deltaOutput
                self.hiddenWeights -= deltaHidden
                self.outputWeights -= deltaOutput

            # Get accuracy for training and test data
            self.trainAccuracy.append(self.get_accuracy(inputs, targets))

            print("epoch ", i+1)

    def forward(self, inputs):
        self.hidden = np.dot(inputs, self.hiddenWeights)
        self.hidden = 1.0 / (1.0 + np.exp(-1*self.hidden)) # Activation function at hidden nodes
        self.hidden = np.concatenate((self.hidden, np.ones((np.shape(inputs)[0], 1))), axis=1)

        outputs = np.dot(self.hidden, self.outputWeights)

        return 1.0 / (1.0 + np.exp(-1*outputs)) # Activation function at output nodes

    def get_accuracy(self, inputs, targets):
        self.outputs = self.forward(inputs)
        correct = np.sum(np.where(targets == np.where(self.outputs > 0.5, 0.9, 0.1), 1, 0)) # Count number of correctly classified inputs
        acc = correct / (np.shape(targets)[1]*np.shape(targets)[0])

        return acc

    def confusionMatrix(self, inputs, targets):
        inputs = np.concatenate((inputs, -np.ones((np.shape(inputs)[0], 1))), axis=1)
        outputs = self.forward(inputs)

        nclasses = np.shape(targets)[1]
        outputs = np.argmax(outputs, 1)
        targets = np.argmax(targets, 1)

        cm = np.zeros((nclasses, nclasses))
        for i in range(nclasses):
            for j in range(nclasses):
                cm[i, j] = np.sum(np.where(outputs == i, 1, 0) * np.where(targets == j, 1, 0))

        return cm