from tensorflow import keras
from keras.models import Sequential
from keras import layers
import matplotlib.pyplot as plt
import numpy as np

# Experiments 1-3

resolutions = [24, 48, 96]
i = 1
for res in resolutions:
    img = np.load(str(res)+'x'+str(res)+'training_images.npy')
    trg = np.load(str(res)+'x'+str(res)+'training_targets.npy')

    train_imgs = img[:int(.8*res**2),:]
    train_targs = trg[:int(.8*res**2),:]
    test_imgs = img[int(.8*res**2):,:]
    test_targs = trg[int(.8*res**2):,:]
    opt = keras.optimizers.SGD(learning_rate=0.01, momentum=0.9)
    eps = 100

    model = Sequential([
            layers.Dense(100, activation='sigmoid', input_shape=(4*res*res,)),
            layers.Dense(units=res*res, activation='sigmoid')
        ])
    model.compile(optimizer=opt, loss='binary_crossentropy', metrics=[keras.metrics.BinaryAccuracy()])

    history = model.fit(train_imgs, train_targs, batch_size=10, epochs=eps, validation_data=(test_imgs, test_targs))

    plt.plot(range(eps), history.history['binary_accuracy'], label='Training Accuracy')
    plt.plot(range(eps), history.history['val_binary_accuracy'], label='Validation Accuracy')
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.ylim(0.1,0.9)
    plt.legend(loc='lower right')
    plt.text(50,0.5,'final validation accuracy:'+str(round(history.history['val_binary_accuracy'][-1]*100,2))+'%')
    plt.savefig("C:/Users/Ross Morrison/Pictures/cs445/"+str(res)+'x'+str(res)+"_exp"+str(i)+"_results.png", format='png')
    plt.show()
    i += 1

# Experiments 4-6

res = 24
learning_rates = [0.01, 0.001, 0.0001]
for lr in learning_rates:
    img = np.load(str(res)+'x'+str(res)+'training_images.npy')
    trg = np.load(str(res)+'x'+str(res)+'training_targets.npy')

    train_imgs = img[:int(.8*res**2),:]
    train_targs = trg[:int(.8*res**2),:]
    test_imgs = img[int(.8*res**2):,:]
    test_targs = trg[int(.8*res**2):,:]
    opt = keras.optimizers.SGD(learning_rate=lr, momentum=0.9)
    eps = 100

    model = Sequential([
            layers.Dense(100, activation='sigmoid', input_shape=(4*res*res,)),
            layers.Dense(units=res*res, activation='sigmoid')
        ])
    model.compile(optimizer=opt, loss='binary_crossentropy', metrics=[keras.metrics.BinaryAccuracy()])

    history = model.fit(train_imgs, train_targs, batch_size=10, epochs=eps, validation_data=(test_imgs, test_targs))

    plt.plot(range(eps), history.history['binary_accuracy'], label='Training Accuracy')
    plt.plot(range(eps), history.history['val_binary_accuracy'], label='Validation Accuracy')
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.ylim(0.1,0.9)
    plt.legend(loc='lower right')
    plt.text(50,0.5,'final validation accuracy:'+str(round(history.history['val_binary_accuracy'][-1]*100,2))+'%')
    plt.savefig("C:/Users/Ross Morrison/Pictures/cs445/"+str(res)+'x'+str(res)+"_exp"+str(i)+"_results.png", format='png')
    plt.show()
    i += 1

# Experiments 7-9

res = 24
lr = 0.01
batch_sizes = [1, 5, 20]
for bs in batch_sizes:
    img = np.load(str(res)+'x'+str(res)+'training_images.npy')
    trg = np.load(str(res)+'x'+str(res)+'training_targets.npy')

    train_imgs = img[:int(.8*res**2),:]
    train_targs = trg[:int(.8*res**2),:]
    test_imgs = img[int(.8*res**2):,:]
    test_targs = trg[int(.8*res**2):,:]
    opt = keras.optimizers.SGD(learning_rate=lr, momentum=0.9)
    eps = 100

    model = Sequential([
            layers.Dense(100, activation='sigmoid', input_shape=(4*res*res,)),
            layers.Dense(units=res*res, activation='sigmoid')
        ])
    model.compile(optimizer=opt, loss='binary_crossentropy', metrics=[keras.metrics.BinaryAccuracy()])

    history = model.fit(train_imgs, train_targs, batch_size=bs, epochs=eps, validation_data=(test_imgs, test_targs))

    plt.plot(range(eps), history.history['binary_accuracy'], label='Training Accuracy')
    plt.plot(range(eps), history.history['val_binary_accuracy'], label='Validation Accuracy')
    plt.xlabel('epoch')
    plt.ylabel('accuracy')
    plt.ylim(0.1,0.9)
    plt.legend(loc='lower right')
    plt.text(50,0.5,'final validation accuracy:'+str(round(history.history['val_binary_accuracy'][-1]*100,2))+'%')
    plt.savefig("C:/Users/Ross Morrison/Pictures/cs445/"+str(res)+'x'+str(res)+"_exp"+str(i)+"_results.png", format='png')
    plt.show()
    i += 1
