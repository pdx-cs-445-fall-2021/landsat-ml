import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
from keras.callbacks import ModelCheckpoint
from tensorflow.keras.optimizers import RMSprop
from sklearn.metrics import confusion_matrix
import warnings
import pathlib
warnings.simplefilter(action='ignore', category=FutureWarning)
from datetime import datetime
import pickle as pkl

def train(img_size=(48,48), lr=0.001, batch_sz=10, epochs=100, tag=""):
    now = datetime.now()
    resume = False

    # img_size = (48,48)
    # img_size = img_size
    # LR = 0.01
    # batch_sz = 10
    # epochs = 100
    save_weights = now.strftime("%m%d%Y_%H%M")+"_"+str(img_size[0])+"_"+str(img_size[1])+tag+"_unet_weights.h5"
    hist_file = now.strftime("%m%d%Y_%H%M")+"_"+str(img_size[0])+"_"+str(img_size[1])+tag+"_history.pkl"
    eval_file = now.strftime("%m%d%Y_%H%M")+"_"+str(img_size[0])+"_"+str(img_size[1])+tag+"_evaluation.pkl"
    load_weights = ""

    def loadData(path, size):
        path = pathlib.Path(path)
        train_img = np.load(path / (str(size[0])+"_"+str(size[1])+"_train_img.npy"))
        train_msk = np.load(path / (str(size[0])+"_"+str(size[1])+"_train_msk.npy"))
        val_img   = np.load(path / (str(size[0])+"_"+str(size[1])+"_val_img.npy"))
        val_msk   = np.load(path / (str(size[0])+"_"+str(size[1])+"_val_msk.npy"))
        tst_img   = np.load(path / (str(size[0])+"_"+str(size[1])+"_tst_img.npy"))
        tst_msk   = np.load(path / (str(size[0])+"_"+str(size[1])+"_tst_msk.npy"))
        return train_img, train_msk, val_img, val_msk, tst_img, tst_msk  

    train_img, train_msk, val_img, val_msk, tst_img, tst_msk = loadData(".", img_size)

    # vgg16_model = tf.keras.applications.vgg16.VGG16()

    # model = Sequential()
    # for layer in vgg16_model.layers[:-1]:
    #     model.add(layer)

    def get_model(img_size, num_classes):
        inputs = keras.Input(shape=img_size + (4,))

        ### [First half of the network: downsampling inputs] ###

        # Entry block
        x = layers.Conv2D(32, 3, strides=2, padding="same")(inputs)
        x = layers.BatchNormalization()(x)
        x = layers.Activation("relu")(x)

        previous_block_activation = x  # Set aside residual

        # Blocks 1, 2, 3 are identical apart from the feature depth.
        for filters in [64, 128, 256]:
            x = layers.Activation("relu")(x)
            x = layers.SeparableConv2D(filters, 3, padding="same")(x)
            x = layers.BatchNormalization()(x)

            x = layers.Activation("relu")(x)
            x = layers.SeparableConv2D(filters, 3, padding="same")(x)
            x = layers.BatchNormalization()(x)

            x = layers.MaxPooling2D(3, strides=2, padding="same")(x)

            # Project residual
            residual = layers.Conv2D(filters, 1, strides=2, padding="same")(
                previous_block_activation
            )
            x = layers.add([x, residual])  # Add back residual
            previous_block_activation = x  # Set aside next residual

        ### [Second half of the network: upsampling inputs] ###

        for filters in [256, 128, 64, 32]:
            x = layers.Activation("relu")(x)
            x = layers.Conv2DTranspose(filters, 3, padding="same")(x)
            x = layers.BatchNormalization()(x)

            x = layers.Activation("relu")(x)
            x = layers.Conv2DTranspose(filters, 3, padding="same")(x)
            x = layers.BatchNormalization()(x)

            x = layers.UpSampling2D(2)(x)

            # Project residual
            residual = layers.UpSampling2D(2)(previous_block_activation)
            residual = layers.Conv2D(filters, 1, padding="same")(residual)
            x = layers.add([x, residual])  # Add back residual
            previous_block_activation = x  # Set aside next residual

        # Add a per-pixel classification layer
        # outputs = layers.Conv2D(num_classes, 3, activation="softmax", padding="same")(x)
        outputs = layers.Conv2D(num_classes, 3, activation="sigmoid", padding="same")(x)


        # Define the model
        model = keras.Model(inputs, outputs)
        return model


    # Free up RAM in case the model definition cells were run multiple times
    keras.backend.clear_session()

    # Build model
    model = get_model(img_size, 1)
    # model.summary()

    model.compile(optimizer=RMSprop(learning_rate=lr), loss='binary_crossentropy', metrics=['accuracy'])

    model_checkpoint = ModelCheckpoint(save_weights, monitor='val_loss', save_best_only=True)

    if (resume):
        model.load_weights(load_weights)

    history = model.fit(x=train_img, y=train_msk, batch_size=batch_sz, epochs=epochs, steps_per_epoch=np.floor(len(train_img)/batch_sz), verbose=2, validation_data=(val_img, val_msk), validation_steps=np.floor(len(val_img)/batch_sz), validation_batch_size=batch_sz, callbacks=[model_checkpoint])

    evaluation = model.evaluate(x=tst_img, y=tst_msk, batch_size=batch_sz, verbose=2, return_dict=True)

    with open(hist_file, "wb") as f:
        pkl.dump(history.history, f)
    with open(eval_file, "wb") as f:
        pkl.dump(evaluation, f)


# for res in [(48, 48), (96,96)]:
#     train(img_size=res, lr=0.01, tag='_res_')

for lr in [0.001, 0.0001]:
    train(img_size=(48,48), lr=lr, tag='_lr'+str(lr).replace(".","_")+"_")

# for batch_sz in [1, 5, 20]:
#     train(batch_sz=batch_sz, tag="_bs"+str(batch_sz)+"_")