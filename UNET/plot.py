import pickle as pkl
import matplotlib.pyplot as plt

def generateTrainingPlots(hist_path, title=''):
    with open(hist_path, 'rb') as f:
        hist = pkl.load(f)
    
    plt.plot(hist.history['accuracy'], label='training set')
    plt.plot(hist.history['val_accuracy'], label='validation set')

    plt.title(title)
    plt.xlabel("Epoch")
    plt.ylabel("Accuracy")
    plt.ylim([0.7, 1.0])
    plt.legend(loc='lower right')
    plt.show()

