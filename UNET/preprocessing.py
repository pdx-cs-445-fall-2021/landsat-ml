import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from skimage.io import imread
from skimage.transform import resize
import matplotlib.pyplot as plt
import warnings
import pathlib
from tqdm import tqdm
import pandas as pd
warnings.simplefilter(action='ignore', category=FutureWarning)

dataset_root = pathlib.Path("C:/Users/cfaber/cs545/cloud_dataset")
training_dir = dataset_root / "38-Cloud_training"
train_patches_csv = "training_patches_38-cloud.csv"
split_ratio = {"test": 0.2, "val": 0.1, "train": 0.7}
val_ratio = 0.3

img_rows = 48
img_cols = 48

def get_input_image_names(list_names, directory_name, if_train=True):
    list_img = []
    list_msk = []
    list_test_ids = []
    
    directory_name = str(directory_name)

    for filenames in tqdm(list_names['name'], miniters=1000):
        nred = 'red_' + filenames
        nblue = 'blue_' + filenames
        ngreen = 'green_' + filenames
        nnir = 'nir_' + filenames

        if if_train:
            dir_type_name = "train"
            fl_img = []
            nmask = 'gt_' + filenames
            fl_msk = directory_name + '/train_gt/' + '{}.TIF'.format(nmask)
            list_msk.append(fl_msk)

        else:
            dir_type_name = "test"
            fl_img = []
            fl_id = '{}.TIF'.format(filenames)
            list_test_ids.append(fl_id)

        fl_img_red = directory_name + '/' + dir_type_name + '_red/' + '{}.TIF'.format(nred)
        fl_img_green = directory_name + '/' + dir_type_name + '_green/' + '{}.TIF'.format(ngreen)
        fl_img_blue = directory_name + '/' + dir_type_name + '_blue/' + '{}.TIF'.format(nblue)
        fl_img_nir = directory_name + '/' + dir_type_name + '_nir/' + '{}.TIF'.format(nnir)
        fl_img.append(fl_img_red)
        fl_img.append(fl_img_green)
        fl_img.append(fl_img_blue)
        fl_img.append(fl_img_nir)

        list_img.append(fl_img)

    if if_train:
        return list_img, list_msk
    else:
        return list_img, list_test_ids

df_train_img = pd.read_csv(training_dir / train_patches_csv)
train_img, train_msk = get_input_image_names(df_train_img, training_dir, if_train=True)
train_img, tst_img_split, train_msk, tst_msk_split = train_test_split(train_img, train_msk, test_size=split_ratio["test"], random_state=42, shuffle=True)
train_img_split, val_img_split, train_msk_split, val_msk_split = train_test_split(train_img, train_msk, test_size=(split_ratio["val"]/(1-split_ratio["test"])), random_state=42, shuffle=True)

train_zip = list(zip(train_img_split, train_msk_split))
test_zip = list(zip(tst_img_split, tst_msk_split))
valid_zip = list(zip(val_img_split, val_msk_split))

def genData(zip_list, img_rows, img_cols):
    image_list = []
    mask_list = []
    
    for file, mask in zip_list:
        img_red = imread(file[0])
        img_green = imread(file[1])
        img_blue = imread(file[2])
        img_nir = imread(file[3])
        
        mask = imread(mask)
        
        image = np.stack((img_red, img_green, img_blue, img_nir), axis=-1)
        image = resize(image, (img_rows, img_cols), preserve_range=True, mode='symmetric')
        mask = resize(mask, (img_rows, img_cols), preserve_range=True, mode='symmetric')
        
        mask = mask[...,np.newaxis]
        mask /= 255
        image /= 65536
        image_list.append(image)
        mask_list.append(mask)
    
    image_list = np.array(image_list)
    mask_list = np.array(mask_list)
    return image_list, mask_list

train_img, train_msk = genData(train_zip, img_rows, img_cols)
val_img, val_msk = genData(valid_zip, img_rows, img_cols)
tst_img, tst_msk = genData(test_zip, img_rows, img_cols)

np.save(str(img_rows)+"_"+str(img_cols)+"_train_img.npy", train_img)
np.save(str(img_rows)+"_"+str(img_cols)+"_train_msk.npy", train_msk)
np.save(str(img_rows)+"_"+str(img_cols)+"_val_img.npy", val_img)
np.save(str(img_rows)+"_"+str(img_cols)+"_val_msk.npy", val_msk)
np.save(str(img_rows)+"_"+str(img_cols)+"_tst_img.npy", tst_img)
np.save(str(img_rows)+"_"+str(img_cols)+"_tst_msk.npy", tst_msk)

# This plots the images int he form of a grid with 1 row and 10 columns
def plotImages(images_arr):
  fig, axes = plt.subplots(1,10,figsize=(20,20))
  axes=axes.flatten()
  for img, ax in zip(images_arr, axes):
    ax.imshow(img)
    ax.axis('off')
  plt.tight_layout()
  plt.show()
  
plotImages(val_img)



