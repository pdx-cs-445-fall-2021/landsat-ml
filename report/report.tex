\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage{tabularx}
\usepackage{array}
\usepackage{amsmath}
\usepackage{breqn}
\usepackage{tikz}
\usetikzlibrary{automata,positioning, arrows}
\usepackage{hyperref}
\usepackage{graphicx} 
\usepackage{float}
\usepackage[center]{caption}
\usepackage{multirow}
\usepackage{textcomp}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage{subcaption}	% You need this package for subfigures


\usepackage{booktabs}

 \captionsetup[figure]{labelfont={bf},name={Fig.},labelsep=period}


\begin{document}
	
\author{
	Embrey, Nick\\
	\texttt{embrey@pdx.edu}
	\and
	Morrison, Ross\\
	\texttt{ros25@pdx.edu}
	\and
	Crippen, Lee\\
	\texttt{lcrippen@pdx.edu}
	\and
	Faber, Chuck\\
	\texttt{cfaber@pdx.edu}
	\and
	Nair, Aakarsh\\
	\texttt{aakarsh@pdx.edu}
}

\date{December 2021}

\title{LandSat-ML: Cloud Cover Segmentation in Satellite Imagery}

\maketitle
\section{Contributions}

\begin{itemize}
	\item \textbf{Nick Embrey}: CNN code development, co-writing CNN section, report editing.
	\item \textbf{Ross Morrison}: MLP code development, Google Colab experimentation, MLP writeup, report editing.
	\item \textbf{Lee Crippen}: Experimentation and training CNN code from original paper with Google Colab, project CNN testing and validation, and report editing.
	\item \textbf{Chuck Faber}: Experimental design/model comparison, U-Net code development, general algorithm testing, report contributions.
	\item \textbf{Aakarsh Nair}: CNN Model Evaluation and Validation, Google Colab Experiments, Report Preparation.
\end{itemize}

\section{Introduction}

Satellite imagery of the earth is more widely and easily available than ever before and represents a vast source of information that can support fields ranging from agriculture to climate to security. However, this opportunity also presents the classic problems of big data, complexity and intractability, and requires tools that allow satellite imagery to be analyzed productively. Cloud segmentation is a preparatory task for doing data science workflows on satellite imagery and involves segmenting images into regions that contain clouds and regions that do not so that these regions can be used for further analysis.

In this project, we perform cloud segmentation on a dataset using two different approaches: using a multilayer perceptron (MLP), and using a convolutional neural network (CNN). We also detail the architecture we used to create these two models, compare their performance using a group of experiments, and discuss our results as well as what we learned from the process.


\subsection{Dataset}


We used an academic dataset called 38-Cloud for this project \cite{38-cloud-1}, which consists of 38 scenes adapted from imagery captured by Landsat 8 satellite. These 38 images use 4 of the 9 spectral bands available from Landsat 8: Red, Green, Blue and Near Infrared. Each scene is an image of size 7601 × 7601 pixels. They are cropped into 384x384 pixel patches, with 8400 patches available for training and 9201 patches available for testing. For this project, we opted to use only the 8400 training patches for training, validation, and testing, since the 9201 designated testing patches require significant additional preprocessing in order to produce usable ground truths. Sample image patches for each spectral band in grayscale, as well as the corresponding ground truth patch and an illustrative false color patch are shown in  Figure \ref{fig:bands}.


\begin{figure}[htbp]
	\centering
	\begin{minipage}[t]{.3\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/red_patch_192_10_by_12_LC08_L1TP_002053_20160520_20170324_01_T1.jpeg}
		\caption{Red}
		\label{fig:sub1}
	\end{minipage}
	\begin{minipage}[t]{.3\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/blue_patch_192_10_by_12_LC08_L1TP_002053_20160520_20170324_01_T1.jpeg}
		\caption{Blue}
		\label{Blue}
	\end{minipage}
	\begin{minipage}[t]{.3\linewidth}
	\centering
	\includegraphics[width=\textwidth]{./images/green_patch_192_10_by_12_LC08_L1TP_002053_20160520_20170324_01_T1.jpeg}
	\caption{Green}
	\label{fig:Green}
	\end{minipage}
	\begin{minipage}[t]{.3\linewidth}
	\centering
	\includegraphics[width=\textwidth]{./images/nir_patch_192_10_by_12_LC08_L1TP_002053_20160520_20170324_01_T1.jpeg}
	\caption{Near Infra-Red}
	\end{minipage}
	\begin{minipage}[t]{.3\linewidth}
	\centering
	\includegraphics[width=\textwidth]{./images/truecolor_patch_192_10_by_12_LC08_L1TP_002053_20160520_20170324_01_T1.jpeg}
	\caption{True Color}
	\label{fig:True Color}
	\end{minipage}
	\begin{minipage}[t]{.3\linewidth}
	\centering
	\includegraphics[width=\textwidth]{./images/gt_patch_192_10_by_12_LC08_L1TP_002053_20160520_20170324_01_T1.jpeg}
	\caption{Ground Truth}
	\label{fig:Ground Truth}
	\end{minipage}
	\caption{Sample of patches across spectral bands}
	\label{fig:bands}
\end{figure}

\begin{center}
    \begin{figure}[htbp]
    \begin{center}
	\begin{tabular}{||c l c||} 
		\hline
		Band  \# & Name & Spectral-Range (nm) \\ [0.5ex] 
		\hline\hline
		1 & Blue &  450-515\\ 
		\hline
		2 & Green & 520-600 \\
		\hline
		3 & Red & 630-680  \\
		\hline
		4 & Near Infrared & 845-885 \\
		\hline
	\end{tabular}
	\end{center}
	\caption{Spectral ranges of dataset image patch bands.}
	\label{fig:ranges}
	\end{figure}
\end{center}


\subsection{Preprocessing}

The four spectral band images for each image patch were scaled to the desired resolution and combined so that all spectral bands for a given patch were represented in one array object. The pixel values for each band were originally 16-bit, so they were divided by 65535 to transform them into float values between 0 and 1.

Ground truth mask patches were also first scaled to the desired resolution. Ground truth pixel values were originally 8-bit, with a value of either 0 or 255, but after scaling, pixel values ranged from 0 to 255 depending on the values of the pixels that were downsampled to create the scaled image. Just as above, these pixels were divided by 255 to transform them to float values between 0 and 1. We then thresholded these ground truth pixel values so that any values greater than or equal to 0.5 were rounded up to 1 and any values less than 0.5 were rounded down to 0.

The 8400 image patches were divided with a 70/10/20 split between training, validation, and testing data, respectively.


\section{Multi-Layer Perceptron}

\subsection{Background} 

The Multilayer Perceptron (MLP) or neural network (NN) is a type of machine learning model founded on Dr. Frank Rosenblatt’s development of the Perceptron \cite{rosenblattsperceptron}. The Perceptron is a linear classifier inspired by a biological neuron, which combines a set of built-in weights with various feature vectors in order to generate a prediction about those features. By continuously feeding feature vectors through the perceptron, it is possible to update the weights and improve predictions. Although this model was innovative, an infamous 1969 report by Minsky and Papert demonstrated that it could only solve linearly separable problems, which led to a decline in interest in the Perceptron for more than a decade \cite{minskyandpapert}.

However, in 1986 there was a renaissance in interest and research in the Perceptron after Rumelhart, Hinton, and McClelland developed the backpropagation algorithm \cite{backprop}. Backpropagation allows a network of multiple layers of Perceptrons (i.e., a Multilayer Perceptron) to adjust weights in each layer based on the error at the output layer. Combining backpropagation with continuous activation functions enables the network to represent any function. The result is a powerful machine learning tool.


\subsection{Architecture}


The MLP used in this project is built with the Keras library. It consists of an input layer, two hidden layers with 50 nodes each, and an output layer. The size of the input and output layers depends on the resolution of the input images. Since images are flattened before entry and there are four spectral bands per image, the number of input nodes is $4(\text{image side length})^2$. Each output node corresponds to a pixel in the target mask, so there are $(\text{image side length})^2$ output nodes. Each layer utilizes a ReLU activation function, except for the final layer which uses a sigmoid activation function. The model utilizes gradient descent with a momentum of 0.9 for optimization and binary cross entropy for accuracy computation. Binary cross-entropy is used because it allows us to compute the error on a pixel-by-pixel basis.


\section{Convolutional Neural Network}

\subsection{Background}
Early work on convolutional neural networks were biologically inspired by investigation in the 50s by Hubel and Wisel in their investigation of the visual cortex of cat \cite{hubel20208}. Here simple cells were seen to respond to edges and bars in particular orientation while complex cells would respond to edges and bars in a spatially invariant manner by summing over the input of several localized simple cells. This concept of simple detectors used to create more  complex detector formed the fundamental basis of convolution neural network models.

The first modern use of artificial convolutional neural networks was implemented in the 90s by Yann LeCun et al. in their paper Gradient-Based Learning Applied to Document Recognition\cite{726791}.   Which demonstrated a CNN which could combine simple feature detectors which are aggregated into more complicated features allowing the network to recognize handwritten images.

They gained further popularity after showing impressive performance in 2012 dataset  on the ImageNet database \cite{NIPS2012_c399862d}. Where ImageNet was a database of about 14 million labeled natural images with the task of the network to predict the label when provided with the image.

\subsection{Architecture}


The convolutional neural network was also created using the popular deep learning library Keras\cite{chollet2015keras}.

We were originally inspired by the U-Net architecture \cite{ronneberger2015unet}, and began with an abbreviated form of this architecture. We started by using a series of convolutions and pooling steps forming the first half of the network and transpose convolutions forming the second half. However, we found that this design had a significant adverse impact on the accuracy of the model.

The network that we ultimately designed is a sequential model with four initial 2D convolutional layers, each with a kernel size of 3x3, stride of 1, ReLU activation functions, and “same” padding. The first two of these layers have 8 filters while the second two have 16. These four layers are followed by a single output layer with a 1x1 convolution and a single filter, using a sigmoid activation function. The Adam optimizer was used with the binary cross-entropy loss function.

Significantly, we found that the max-pooling layers had a strong adverse effect on accuracy. We suspect this may be due to the relatively low resolution of the images we used, but we also found some indication in one paper that max-pooling does not work well for cloud segmentation \cite{2019CloudClassification}. We also found that using a sigmoid activation function at the output layer significantly increased the accuracy of the model, something we were inspired to try after seeing this technique used in the Cloud-Net paper for which our dataset was produced.


\section{Comparison and Experimental Results}

For our experiments, our three major variables between comparing the MLP and the CNN were image resolution, learning rate, and batch size. Results are summarized below in Figure \ref{fig:experiment-summary}.

\begin{figure}[htb]
\centering
		\includegraphics[width=\textwidth]{./images/datatable2.png}
		\caption{Experiment Summary}
		\label{fig:experiment-summary}
\end{figure}

\subsection{Image Resolution}

While the original Cloud-38 image patches had a resolution of 384x384 pixels, this resolution would have taken too long for the purposes of this project, given the resources we had available. Instead we tested three different resolutions: 24x24, 48x48, and 96x96 and scaled the input images as necessary for this purpose. We trained each model with these resolutions, and used a batch size of 10, learning rate of 0.01, and 100 epochs.

In terms of the image resolution, The MLP had the best results with the 24x24 sized images, with an accuracy of 0.8566. The CNN in contrast had the best accuracy with 48x48 sized images, with an accuracy of 0.9496, though it was fairly close to the 96x96 accuracy value. We opted to work with 48x48 for the remainder of the tests, as the time per epoch of working with 96x96 sized images was significantly larger.


\subsection{Learning Rate}

After running experiments with these three resolutions, we decided upon a common resolution to complete the remaining tests with. We decided to use the 48x48 resolution as it had comparably good accuracy for both networks. We then tested several learning rate values. Including the value of 0.01 we tested with the image resolution tests, we also tested learning rates of 0.001, and 0.0001. Again we kept the batch size at 10, and the number of epochs to 100.

For the MLP, the learning rate which produced the highest accuracy was ‘0.01’, with an accuracy of 0.8190. However, for the CNN the learning rate with the highest accuracy was ‘0.001’ with an accuracy of 0.9553. What was interesting to see here was that the MLP accuracy very clearly declined as the learning rate became more granular (smaller).


\subsection{Batch Size}

Finally we tested several different batch sizes and their effect on the accuracy of the models. We chose a common learning rate of 0.001 for these tests, and continued to work with 48x48 sized images, and 100 epochs, but we iterated through batch sizes of 1, 5, and 20.

This group of experiments, all with an image resolution of 48x48, learning rate of 0.001, and 100 epochs, actually produced the best overall accuracy for the CNN and the second best overall accuracy for the MLP. The MLP did best with a batch size of 1 with an accuracy of 0.8517, while the CNN performed best with a batch size of 5 and produced an accuracy of 0.9609.

\subsection{Comparison}

It was surprising to see that even with a very simple architecture of our CNN, it outpaced the MLP in every test by about 10-20\% accuracy. Interestingly what we saw was that what produced better accuracy results for the MLP didn’t necessarily produce the best accuracy results for the CNN and vice-versa.

The paper from Mohajerani and Saeedi surprisingly only had marginal improvements in accuracy compared to ours (96.09\% in our network vs 96.48\% in their paper) and with a much more complicated network that required much longer training times. It’s possible that perhaps training on cloud segmentation is relatively easy for CNNs and more complicated networks generally offer only small improvements, or perhaps there is something else at play here that we are missing.

We also had to keep in mind that in the total dataset, the overall ratio of not-cloud to cloud pixels was about 70\% not cloud, to 30\% cloud cover. A network that might just classify everything as ‘not-cloud’ would still be about 70\% accurate, and we saw such behavior in some early iterations of our network.   See Figure \ref{fig:mlp-performance} for a MLP performance tracked over epochs for the above experimental values and Figure \ref{fig:cnn-performance} the corresponding experimental values.

\begin{figure}[htb]
	\vspace*{-3cm}
	\caption{MLP Learning Curves For Various Image Resolutions and Hyper-Parameters}
	\label{fig:mlp-performance}
  \begin{tabular*}{\textwidth}{@{} c @{\extracolsep{\fill}} c @{}}
	\begin{minipage}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/alt_mlp_24x24_exp1_results.png}
	\end{minipage}&
	\begin{minipage}[t]{0.45\linewidth}
			 \centering
			 \includegraphics[width=\textwidth]{./images/alt_mlp_48x48_exp2+4_results.png} 
	\end{minipage}\\
	\begin{minipage}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/alt_mlp_96x96_exp3_results.png}
	\end{minipage}&
	\begin{minipage}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/alt_mlp_48x48_exp5_results.png}
	\end{minipage}\\
	\begin{minipage}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/alt_mlp_48x48_exp6_results.png}
	\end{minipage}&
	\begin{minipage}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/alt_mlp_48x48_exp7_results.png}
	\end{minipage}\\
	\begin{minipage}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/alt_mlp_48x48_exp8_results.png}
	\end{minipage}&
	\begin{minipage}[t]{0.45\linewidth}
		\centering
		\includegraphics[width=\textwidth]{./images/alt_mlp_48x48_exp9_results.png}\\
	\end{minipage}
	\end{tabular*}
\end{figure}

\begin{figure}[htb]
	\caption{CNN Learning Curves For Various Image Resolutions and Hyper-Parameters}
	\label{fig:cnn-performance}
	\begin{tabular*}{\textwidth}{@{} c @{\extracolsep{\fill}} c @{}}
		\includegraphics[width=0.45\textwidth]{./images/cnn_24x24_exp1_results.png}&
		\includegraphics[width=0.45\textwidth]{./images/cnn_48x48_exp2+4_results.png}\\
		\includegraphics[width=0.45\textwidth]{./images/cnn_96x96_exp3_results.png}&
		\includegraphics[width=0.45\textwidth]{./images/cnn_48x48_exp5_results.png}\\
		\includegraphics[width=0.45\textwidth]{./images/cnn_48x48_exp6_results.png}&
		\includegraphics[width=0.45\textwidth]{./images/cnn_48x48_exp7_results.png}\\
		\includegraphics[width=0.45\textwidth]{./images/cnn_48x48_exp8_results.png}&
		\includegraphics[width=0.45\textwidth]{./images/cnn_48x48_exp9_results.png}
	\end{tabular*}

\end{figure}

\section{Discussion}

One of the challenges we faced was working with the Keras library which none of us had much experience with. We all eventually got used to the general method of constructing different models by adding layers and defining their characteristics, before compiling it, and then running the fit() function to train the model. 

Another challenge we faced was the difficulty of working with a multi-spectral dataset, which few of us had done before. The dataset included four spectral channels for each satellite image patch, which had to be associated with one target mask. 

A third challenge was finding the hardware to run our models on. Not everyone in the group had a computer with a GPU. We attempted to do some runs in Google Colab which provides the use of a GPU if the pro edition is purchased but other limitations made it difficult, including the speed of file I/O through Google Drive, and the fact that it will end a session if a time-out duration has been reached. 

With more time, we might try other types of networks with this dataset, including pre-made CNN architectures, like U-Net which is a convolutional neural network architecture developed for biomedical image segmentation. We attempted to get a U-net architecture working to have a third model to compare against, but we were not confident in our setup and the results from it. Our results were typically accurate in the 85\% range and since not enough time was spent to try to understand this architecture, it’s likely that the processing and/or setup was incorrect. We did find that accuracy of this network increased significantly with the image resolution, however the training time also increased greatly and we couldn’t fully test it with larger images for this project within the amount of time we had.

\bibliography{references} 
\bibliographystyle{ieeetr}


\end{document}
	
