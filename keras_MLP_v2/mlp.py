import tensorflow as tf
from tensorflow.keras import layers, models
import matplotlib.pyplot as plt
import numpy as np
import tensorflow.keras.optimizers as opt

root_path = "/Users/nick/dev/ml-2021/38-cloud-project/"

side_length = 48
resolution = "{}x{}".format(side_length, side_length)
learning_rate = 0.001
batch_size = 20

training_images = np.load(root_path + resolution + "_training-images.npy").reshape(5880, -1)
training_targets = np.load(root_path + resolution + "_training-targets.npy").reshape(5880, -1)

validation_images = np.load(root_path + resolution + "_validation-images.npy").reshape(840, -1)
validation_targets = np.load(root_path + resolution + "_validation-targets.npy").reshape(840, -1)

testing_images = np.load(root_path + resolution + "_testing-images.npy").reshape(1680, -1)
testing_targets = np.load(root_path + resolution + "_testing-targets.npy").reshape(1680, -1)

model = models.Sequential([
        layers.Dense(50, activation='relu', input_shape=(training_images[0].shape)),
        layers.Dense(50, activation='relu'),
        layers.Dense(50, activation='relu'),
        layers.Dense(units=side_length*side_length, activation='sigmoid')
    ])

model.compile(
    optimizer=opt.SGD(learning_rate=learning_rate, momentum=0.9),
    loss=tf.keras.losses.BinaryCrossentropy(),
    metrics=tf.keras.metrics.BinaryAccuracy()
)

history = model.fit(
    training_images,
    training_targets,
    batch_size=batch_size, epochs=100,
    validation_data=(validation_images, validation_targets)
)

evaluation = model.evaluate(
    testing_images,
    testing_targets,
    batch_size=batch_size,
    return_dict=True
)

plt.plot(history.history['binary_accuracy'], label='training set')
plt.plot(history.history['val_binary_accuracy'], label='validation set')
plt.title(
    'Resolution: {}, Learning rate: {}, Batch size: {}\nTest accuracy: {:.4f}, Test loss: {:.4f}'
    .format(resolution, learning_rate, batch_size, evaluation['binary_accuracy'], evaluation['loss'])
)
plt.xlabel('Epoch')
plt.ylabel('Accuracy')
plt.ylim([0.5, 1.0])
plt.legend(loc='lower right')
plt.show()